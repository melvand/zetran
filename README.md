# Zetran

* Check more on: [Investor Money Buy Stocks](https://investormoney.com/guides/how-to-buy-stocks-and-shares/)
* Check more on: [Investor Money Buy Cryptocurrency](https://investormoney.com/guides/buying-cryptocurrency/)


## Prerequisites

* AWS account with permissions to the following services: 
  * Amazon VPC, Amazon VPC endpoints, Amazon S3, AWS Secrets Manager, AWS Lambda, Amazon API Gateway
* [AWS CLI](https://aws.amazon.com/cli/)
* [AWS Serverless Application Model (AWS SAM) CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html) to deploy the application
* A Managed Blockchain network that is up and running in one of the supported AWS regions.
* At least one chaincode that is **installed** and **instantiated** on your blockchain network.


## Solution overview
The following diagram illustrates the architecture of the solution.

![Architecture diagram](Solution_Architecture_Diagram.png)

## Security

See [CONTRIBUTING](CONTRIBUTING.md#security-issue-notifications) for more information.

## License

This library is licensed under the MIT-0 License. See the LICENSE file.
